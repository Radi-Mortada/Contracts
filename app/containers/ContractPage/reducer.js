/*
 *
 * ContractPage reducer
 *
 */

import { fromJS } from 'immutable';

export const initialState = fromJS({});

function contractPageReducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default contractPageReducer;
