import { fromJS } from 'immutable';
import contractPageReducer from '../reducer';

describe('contractPageReducer', () => {
  it('returns the initial state', () => {
    expect(contractPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
