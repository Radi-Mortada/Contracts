/**
 *
 * ContractPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import ImmutableProptypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Formik } from 'formik';

import injectSaga from 'utils/injectSaga';

import { makeSelectContractById } from 'containers/App/selectors';
import { loadContractById } from 'containers/App/actions';
import Form from 'components/Form';
import StyledButton from 'components/Button/StyledButton';
import { Input, FormGroup } from 'components/Form/partials';
import saga from './saga';
import { updateContract } from './actions';
import { sagasAndreducerKey } from './constants';

/* eslint-disable react/prefer-stateless-function */
export class ContractPage extends React.PureComponent {
  componentDidMount() {
    const { match } = this.props;

    this.props.getContract(match.params.id);
  }

  render() {
    const { contract, updateContractInvoke } = this.props;

    if (!contract) return null;

    return (
      <div>
        <h1>{`Contract: ${contract.getIn(['user', 'name'])}`}</h1>
        <Formik
          initialValues={contract.toJS()}
          onSubmit={(values, actions) => {
            updateContractInvoke(values, actions);
          }}
          render={({
            handleChange,
            setValues,
            handleBlur,
            values,
            ...props
          }) => (
            <Form onSubmit={props.handleSubmit}>
              <FormGroup>
                <Input
                  type="text"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.user.name}
                  name="user.name"
                />
              </FormGroup>

              <FormGroup>
                <Input
                  type="text"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.user.surname}
                  name="user.surname"
                />
              </FormGroup>

              <FormGroup>
                <Input
                  type="text"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.amountInUsd}
                  name="amountInUsd"
                />
              </FormGroup>

              <FormGroup>
                <Input
                  type="text"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.currency}
                  name="currency"
                />
              </FormGroup>

              <FormGroup>
                <Input
                  type="text"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.date}
                  name="date"
                />
              </FormGroup>
              {props.errors.name && (
                <div id="feedback">{props.errors.name}</div>
              )}
              <FormGroup>
                <StyledButton type="submit">Submit</StyledButton>
              </FormGroup>
            </Form>
          )}
        />
      </div>
    );
  }
}

ContractPage.propTypes = {
  getContract: PropTypes.func,
  updateContractInvoke: PropTypes.func,
  match: PropTypes.object,
  contract: PropTypes.oneOfType([ImmutableProptypes.map, PropTypes.bool]),
};

const mapStateToProps = (state, ownProps) => {
  const { match } = ownProps;
  const selectorsMadeOnce = {
    contract: makeSelectContractById(match.params.id),
  };

  return createStructuredSelector(selectorsMadeOnce)(state);
};

function mapDispatchToProps(dispatch) {
  return {
    getContract: id => dispatch(loadContractById(id)),
    updateContractInvoke: (formValues, formikActions) =>
      dispatch(updateContract(formValues, formikActions)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withSaga = injectSaga({ key: sagasAndreducerKey, saga });

export default compose(
  withSaga,
  withConnect,
)(ContractPage);
