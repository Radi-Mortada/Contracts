/**
 * CRUD the contract.
 */
import { fromJS } from 'immutable';
import { call, put, takeLatest, select } from 'redux-saga/effects';
import { LOAD_CONTRACT_BY_ID } from 'containers/App/constants';
import {
  loadContractByIdSuccess,
  contractLoadByIdError,
} from 'containers/App/actions';
import { makeSelectContractById } from 'containers/App/selectors';

import request from 'utils/request';

import { updateContractSuccess, updateContractError } from './actions';
import { UPDATE_CONTRACT } from './constants';

/**
 * Contract load by `id` handler.
 */
export function* handleLoadContractById(event) {
  const {
    payload: { id },
  } = event;
  const requestURL = `http://localhost:3000/api/contracts/${id}`;

  const contractFromCache = yield select(makeSelectContractById(id));

  // Contract already loaded.
  if (contractFromCache) return;

  try {
    // Call our request helper (see 'utils/request')
    const contract = yield call(request, requestURL);
    yield put(loadContractByIdSuccess(fromJS(contract)));
  } catch (err) {
    yield put(contractLoadByIdError(err));
  }
}

/**
 * Contract modifier handler.
 */
export function* handleUpdateContract(event) {
  const {
    payload: { formValues, formikActions },
  } = event;

  const { id } = formValues;

  const requestURL = `http://localhost:3000/api/contracts/${id}`;

  const requestOptions = { method: 'PUT', body: JSON.stringify(formValues) };

  try {
    // Call our request helper (see 'utils/request')
    const contract = yield call(request, requestURL, requestOptions);

    yield put(updateContractSuccess(fromJS(contract)));
    formikActions.setSubmitting(false);
  } catch (err) {
    yield put(updateContractError(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* contractPageSaga() {
  yield takeLatest(LOAD_CONTRACT_BY_ID, handleLoadContractById);
  yield takeLatest(UPDATE_CONTRACT, handleUpdateContract);
}
