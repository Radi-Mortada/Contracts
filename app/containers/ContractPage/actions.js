/*
 *
 * ContractPage actions
 *
 */

import {
  UPDATE_CONTRACT,
  UPDATE_CONTRACT_SUCCESS,
  UPDATE_CONTRACT_ERROR,
} from './constants';

/**
 * Update `contract` details, this action starts the request saga.
 *
 * @return {object} An action object with a type of UPDATE_CONTRACT.
 */
export function updateContract(formValues, formikActions) {
  return {
    type: UPDATE_CONTRACT,
    payload: {
      formValues,
      formikActions,
    },
  };
}

/**
 * Update the `contract` in reducer.
 *
 * @return {object} An action object with a type of UPDATE_CONTRACT_SUCCESS.
 */
export function updateContractSuccess(contract) {
  return {
    type: UPDATE_CONTRACT_SUCCESS,
    payload: {
      contract,
    },
  };
}

/**
 * Updating `contract` failed
 *
 * @return {object} An action object with a type of UPDATE_CONTRACT_ERROR.
 */
export function updateContractError(error) {
  return {
    type: UPDATE_CONTRACT_ERROR,
    error,
  };
}
