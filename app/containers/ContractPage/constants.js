/*
 *
 * ContractPage constants
 *
 */
export const sagasAndreducerKey = 'contractPage';

export const UPDATE_CONTRACT = 'app/ContractPage/UPDATE_CONTRACT';
export const UPDATE_CONTRACT_SUCCESS =
  'app/ContractPage/UPDATE_CONTRACT_SUCCESS';
export const UPDATE_CONTRACT_ERROR = 'app/ContractPage/UPDATE_CONTRACT_ERROR';
