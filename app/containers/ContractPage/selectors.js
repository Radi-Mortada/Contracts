import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the contractPage state domain
 */

const selectContractPageDomain = state =>
  state.get('contractPage', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by ContractPage
 */

const makeSelectContractPage = () =>
  createSelector(selectContractPageDomain, substate => substate.toJS());

export default makeSelectContractPage;
export { selectContractPageDomain };
