/**
 *
 * Asynchronously loads the component for ContractPage
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
