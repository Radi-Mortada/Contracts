/*
 *
 * HomePage actions
 *
 */

import {
  DELETE_CONTRACT,
  DELETE_CONTRACT_SUCCESS,
  DELETE_CONTRACT_ERROR,
} from './constants';

/**
 * Delete `contract`, this action starts the request saga.
 *
 * @return {object} An action object with a type of DELETE_CONTRACT.
 */
export function deleteContract(id) {
  return {
    type: DELETE_CONTRACT,
    payload: {
      id,
    },
  };
}

/**
 * Delete the `contract` from the reducer.
 *
 * @return {object} An action object with a type of DELETE_CONTRACT_SUCCESS.
 */
export function deleteContractSuccess(id) {
  return {
    type: DELETE_CONTRACT_SUCCESS,
    payload: {
      id,
    },
  };
}

/**
 * Deleting `contract` failed
 *
 * @return {object} An action object with a type of DELETE_CONTRACT_ERROR.
 */
export function deleteContractError(error) {
  return {
    type: DELETE_CONTRACT_ERROR,
    error,
  };
}
