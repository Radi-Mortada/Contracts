/**
 * Gets the contracts.
 */
import { fromJS } from 'immutable';
import { call, put, takeLatest } from 'redux-saga/effects';
import { LOAD_CONTRACTS } from 'containers/App/constants';
import { contractsLoaded, contractsLoadingError } from 'containers/App/actions';
import request from 'utils/request';

import { deleteContractSuccess, deleteContractError } from './actions';
import { DELETE_CONTRACT } from './constants';

/**
 * Contracts request/response handler.
 */
export function* getContracts() {
  const requestURL = `http://localhost:3000/api/contracts`;

  try {
    // Call our request helper (see 'utils/request')
    const response = yield call(request, requestURL);

    const contracts = fromJS(response);

    yield put(contractsLoaded(contracts));
  } catch (err) {
    yield put(contractsLoadingError(err));
  }
}

/**
 * Delete Contract handler.
 */
export function* handleDeleteContract(event) {
  const {
    payload: { id },
  } = event;

  const requestURL = `http://localhost:3000/api/contracts/${id}`;

  const requestOptions = { method: 'DELETE' };

  try {
    // Call our request helper (see 'utils/request')
    yield call(request, requestURL, requestOptions);

    yield put(deleteContractSuccess(id));
  } catch (err) {
    yield put(deleteContractError(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* githubData() {
  // Watches for LOAD_CONTRACTS actions and calls getContracts when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(LOAD_CONTRACTS, getContracts);
  yield takeLatest(DELETE_CONTRACT, handleDeleteContract);
}
