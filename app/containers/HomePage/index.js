/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import ImmutableProptypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import injectSaga from 'utils/injectSaga';
import {
  makeSelectContracts,
  makeSelectLoading,
} from 'containers/App/selectors';
import { loadContracts } from 'containers/App/actions';
import Table from 'components/Table';
import LoadingIndicator from 'components/LoadingIndicator';

import saga from './saga';
import { deleteContract } from './actions';
import { sagasAndreducerKey } from './constants';

/* eslint-disable react/prefer-stateless-function */
export class HomePage extends React.PureComponent {
  componentDidMount() {
    this.props.loadContractsInvoked();
  }

  onEditClick = id => () => {
    const { history } = this.props;

    history.push(`/contract/${id}`);
  };

  onDeleteClick = id => () => {
    const { deleteContractInvoked } = this.props;
    deleteContractInvoked(id);
  };

  render() {
    const { loading, contracts } = this.props;

    if (!contracts) return null;

    return (
      <article>
        <Helmet>
          <title>Home Page</title>
          <meta
            name="description"
            content="A React.js Boilerplate application homepage"
          />
        </Helmet>
        {loading && <LoadingIndicator />}
        {contracts && (
          <Table
            data={contracts}
            onEditClick={this.onEditClick}
            onDeleteClick={this.onDeleteClick}
          />
        )}
      </article>
    );
  }
}

HomePage.propTypes = {
  loading: PropTypes.bool,
  contracts: PropTypes.oneOfType([ImmutableProptypes.list, PropTypes.bool]),
  loadContractsInvoked: PropTypes.func,
  history: PropTypes.object,
  deleteContractInvoked: PropTypes.func,
};

export function mapDispatchToProps(dispatch) {
  return {
    loadContractsInvoked: () => dispatch(loadContracts()),
    deleteContractInvoked: id => dispatch(deleteContract(id)),
  };
}

const mapStateToProps = createStructuredSelector({
  contracts: makeSelectContracts(),
  loading: makeSelectLoading(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withSaga = injectSaga({ key: sagasAndreducerKey, saga });

export default compose(
  withSaga,
  withConnect,
)(HomePage);
