/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable';

import { UPDATE_CONTRACT_SUCCESS } from 'containers/ContractPage/constants';
import { DELETE_CONTRACT_SUCCESS } from 'containers/HomePage/constants';

import {
  LOAD_CONTRACTS_SUCCESS,
  LOAD_CONTRACT_BY_ID_SUCCESS,
} from './constants';

// The initial state of the App
const initialState = fromJS({
  contracts: undefined,
});

function appReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case LOAD_CONTRACTS_SUCCESS: {
      const { contracts } = payload;
      return state.set('contracts', contracts);
    }
    case LOAD_CONTRACT_BY_ID_SUCCESS: {
      const { contract } = payload;
      return state.update(
        'contracts',
        contracts =>
          contracts ? contracts.push(contract) : fromJS([contract]),
      );
    }
    case UPDATE_CONTRACT_SUCCESS: {
      const { contract } = payload;

      return state.updateIn(
        [
          'contracts',
          state
            .get('contracts')
            .findIndex(item => item.get('id') === contract.get('id')),
        ],
        () => contract,
      );
    }
    case DELETE_CONTRACT_SUCCESS: {
      const { id } = payload;
      return state.update('contracts', contracts =>
        contracts.filter(c => c.get('id') !== id),
      );
    }
    default:
      return state;
  }
}

export default appReducer;
