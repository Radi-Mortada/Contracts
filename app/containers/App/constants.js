/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const LOAD_CONTRACTS = 'boilerplate/App/LOAD_CONTRACTS';
export const LOAD_CONTRACTS_SUCCESS = 'boilerplate/App/LOAD_CONTRACTS_SUCCESS';
export const LOAD_CONTRACTS_ERROR = 'boilerplate/App/LOAD_CONTRACTS_ERROR';

export const LOAD_CONTRACT_BY_ID = 'boilerplate/App/LOAD_CONTRACT_BY_ID';
export const LOAD_CONTRACT_BY_ID_SUCCESS =
  'boilerplate/App/LOAD_CONTRACT_BY_ID_SUCCESS';
export const LOAD_CONTRACT_BY_ID_ERROR =
  'boilerplate/App/LOAD_CONTRACT_BY_ID_ERROR';
