/**
 * The global state selectors
 */

import { createSelector } from 'reselect';

const selectGlobal = state => state.get('global');

const selectRouter = state => state.get('router');

const makeSelectLoading = () =>
  createSelector(selectGlobal, globalState => globalState.get('loading'));

const makeSelectError = () =>
  createSelector(selectGlobal, globalState => globalState.get('error'));

const makeSelectLocation = () =>
  createSelector(selectRouter, routerState =>
    routerState.get('location').toJS(),
  );

const makeSelectContracts = () =>
  createSelector(selectGlobal, globalState => {
    const contracts = globalState.get('contracts');
    if (!contracts) return undefined;

    return contracts;
  });

const makeSelectContractById = id =>
  createSelector(selectGlobal, globalState => {
    const contracts = globalState.get('contracts');

    if (!contracts) return undefined;

    return contracts.find(contract => contract.get('id').toString() === id);
  });

export {
  selectGlobal,
  makeSelectLoading,
  makeSelectError,
  makeSelectLocation,
  makeSelectContracts,
  makeSelectContractById,
};
