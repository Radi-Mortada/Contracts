/*
 * App Actions
 *
 */

import {
  LOAD_CONTRACTS,
  LOAD_CONTRACTS_SUCCESS,
  LOAD_CONTRACTS_ERROR,
  LOAD_CONTRACT_BY_ID,
  LOAD_CONTRACT_BY_ID_SUCCESS,
  LOAD_CONTRACT_BY_ID_ERROR,
} from './constants';

/**
 * Load the `contracts`, this action starts the request saga.
 *
 * @return {object} An action object with a type of LOAD_CONTRACTS.
 */
export function loadContracts() {
  return {
    type: LOAD_CONTRACTS,
  };
}

/**
 * Dispatched when the `contracts` are loaded by the request saga.
 *
 * @param  {array} contracts The `contracts` data.
 *
 * @return {object} An action object with a type of LOAD_CONTRACTS_SUCCESS passing the `contracts`.
 */
export function contractsLoaded(contracts) {
  return {
    type: LOAD_CONTRACTS_SUCCESS,
    payload: {
      contracts,
    },
  };
}

/**
 * Dispatched when loading the `contracts` fails.
 *
 * @param  {object} error The error.
 *
 * @return {object} An action object with a type of LOAD_CONTRACTS_ERROR passing the error.
 */
export function contractsLoadingError(error) {
  return {
    type: LOAD_CONTRACTS_ERROR,
    error,
  };
}

/**
 * Load the single `contract` by `id` , this action starts the request saga.
 *
 * @return {object} An action object with a type of LOAD_CONTRACT_BY_ID.
 */
export function loadContractById(id) {
  return {
    type: LOAD_CONTRACT_BY_ID,
    payload: { id },
  };
}

/**
 * Dispatched when we load single `contract` by `id`.
 *
 * @param  {array} contract  The `contract`  data.
 *
 * @return {object} An action object with a type of LOAD_CONTRACT_BY_ID_SUCCESS passing the `contract` .
 */
export function loadContractByIdSuccess(contract) {
  return {
    type: LOAD_CONTRACT_BY_ID_SUCCESS,
    payload: {
      contract,
    },
  };
}

/**
 * Dispatched when loading the `contract` by `id` fails.
 *
 * @param  {object} error The error.
 *
 * @return {object} An action object with a type of LOAD_CONTRACT_BY_ID_ERROR passing the error.
 */
export function contractLoadByIdError(error) {
  return {
    type: LOAD_CONTRACT_BY_ID_ERROR,
    error,
  };
}
