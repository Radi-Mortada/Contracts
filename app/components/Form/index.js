/**
 *
 * Form
 *
 */

import styled from 'styled-components';

const Form = styled.form`
  border: 1px papayawhip solid;
  border-radius: 6px;
  width: 100%;
  padding: 1rem;
  background-color: darkCyan;
`;

Form.propTypes = {};

export default Form;
