### Run the project
#### `yarn` or `npm install`
#### To run the app in development mode
#### `npm start` or `yarn start`
#### To run the project in production mode
#### `yarn build` then `yarn start:prod`
<br>
in development mode open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will automatically reload if you make changes to the code or simply view production build using same url.<br>
You will see the build errors and lint warnings in the console.



### Contracts

Simple way to manage your contracts, you can easily list, modify or remove contracts.
Contracts solution focuses on delievering front-end solution but includes mocked api as back-end service using json-server

### Reasoning behind our technical choices, including architectural

Solution built using several libraries i.e ReactJS, immutableJS, redux, redux-sagas
which provides high performance maintainable project, so we use ReactJS for document update based on virtual dom, we used "Unidirectional flow" design pattern that's based on Flux using redux, the memoization and memory headache free and improvement of javascript performance using immutableJS.

### Trade-offs

It would be nice if we have single url (page) to contain the current logic introducing Popup to modify the contracts.

### Resume

https://www.visualcv.com/radi-ali-mortada
